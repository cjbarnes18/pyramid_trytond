<html>
<head>
  <title>Users List</title>
</head>
<body>

<table>
  <tr>
    <th>Name</th>
    <th>Login</th>
    <th>Email</th>
    <th>Password</th>
  </tr>

  % for user in users:
      ${makerow(user)}
  % endfor
</table>
<%def name="makerow(row)">
  <tr>
    <td>${row.name}</td>
    <td>${row.login}</td>
    <td>${row.email}</td>
    <td>${row.password}</td>
  </tr>
</%def>

</body>
</html>
${renderer.begin(request.resource_url(request.root, 'login'))}
${renderer.csrf_token()}
<div class="form">
  <table>
    <tr>
      <th>User:</th>
      <td>${renderer.select("name", [x.login for x in users])}</td>
      <td>${renderer.errorlist("name")}</td>
    </tr>
    <tr>
      <th>Password:</th>
      <td>${renderer.password("password", size=30)}</td>
      <td>${renderer.errorlist("password")}</td>
    </tr>
    <tr>
      <td>${renderer.submit("submit", "Submit")}</td>
    </tr>
  </table>
</div>
${renderer.end()}

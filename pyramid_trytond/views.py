from pyramid.view import view_config
from pyramid.response import Response
from pyramid.httpexceptions import HTTPFound
#from pyramid.renderers import render_to_response
from pyramid_simpleform import Form
from pyramid_simpleform.renderers import FormRenderer
from .login import LoginSchema, TrytonLogin


@view_config(route_name='home')
def home(request):
    user = request.session.get('name', 'World!')
    return  Response('Hello %s' % user)

@view_config(route_name='login', renderer='pyramid_trytond:templates/login.mak')
def login(request):
    form = Form(request, schema=LoginSchema)
    if form.validate():
        data = form.bind(TrytonLogin(request))
        request.session['name'] = data.name
        request.session['login'] = data.login
        request.session['tryton_session'] = data.session
        return HTTPFound(location='/')
    pool = request.tryton_pool
    user_obj = pool.get('res.user')
    user_search = user_obj.search([
            ('login', '!=', 'admin'), 
            ('active', '=', 'True')])
    user_list = user_obj.browse(user_search)

    return dict(renderer=FormRenderer(form), users=user_list)

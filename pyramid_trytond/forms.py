from formencode import Schema, validators
from trytond import security

class LoginSchema(Schema):
    filter_extra_fields = True
    allow_extra_fields = True
    name = validators.String(not_empty=True)
    password = validators.String(not_empty=True)

class TrytonLogin(object):
    '''Tryton user login validator.'''
    def __init__(self, request):
        self.name = None
        self._password = None
        self._request = request

    @property
    def password(self):
        return self._password
 
    @password.setter
    def password(self, value):
        from hashlib import sha1
        self._password = sha1(value).hexdigest()

    @property
    def login(self):
        if self.name == None:
            return None
        pool = self._request.tryton_pool
        user_obj = pool.get('res.user')
        user = user_obj.search([('name', '=', self.name)])
        return user_obj.browse(user)[0].login
    
    @property
    def session(self):
        return security.login(
            self._request.registry.settings['trytond.database'],
            self.login,
            self.password
            )

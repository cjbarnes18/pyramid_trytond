def includeme(config):
    from trytond.config import CONFIG
    CONFIG.update_etc(config.registry.settings['trytond.config'])
    from trytond.pool import Pool
    Pool.start()
    Pool(config.registry.settings['trytond.database']).init()
    config.set_request_property(get_tryton_pool, 'tryton_pool', reify=True)
    
def get_tryton_pool(request):
    from trytond.transaction import Transaction
    from trytond.pool import Pool
    Transaction().start(request.registry.settings['trytond.database'], 0)
    pool = Pool()
    
    def _cleanup(request):
        Transaction().stop()
    request.add_finished_callback(_cleanup)
    return pool
